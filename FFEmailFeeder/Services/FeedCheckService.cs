﻿using FFEmailFeeder.Data;
using FFEmailFeeder.Database;

using MailKit.Net.Smtp;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using MimeKit;

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FFEmailFeeder.Services;
internal class FeedCheckService(
		IConfiguration configuration,
		IDbContextFactory<FeedDbContext> dbContextFactory,
		ILogger<FeedCheckService> logger,
		SmtpClient emailClient
	) : IFeedCheckService, IDisposable
{
	private bool disposedValue;

	Timer rssCheckRunner;
	List<FeedConfig> feeds;
	List<string> feedIds;

	MailConfiguration emailConfig;

	static readonly Stack<(SyndicationItem item, FeedConfig cfg)> emailQueue = [];
	bool emailsRunning = false;

	Task IFeedCheckService.RunAsync()
	{
		int timerIntervalMillis = configuration.GetRequiredSection("RSS:CheckTick").Get<int>();
		TimeSpan timerInterval = TimeSpan.FromMilliseconds(timerIntervalMillis);

		feeds = configuration.GetRequiredSection("RSS:Feeds").Get<List<FeedConfig>>() ?? [];
		feedIds = [.. feeds.Select(e => e.Id)];

		emailConfig = configuration.GetRequiredSection("Email").Get<MailConfiguration>() ??
			throw new Exception("Failed to load mail settings");

		logger.LogInformation("Feed Check Serivce initalization complete, starting runner.");

		rssCheckRunner = new(async (e) => await CheckFeedsAsync(), null,
			TimeSpan.FromSeconds(5), timerInterval);

		return Task.CompletedTask;
	}

	async Task CheckFeedsAsync()
	{
		logger.LogDebug("Running feed check.");

		await using var dbContext = await dbContextFactory.CreateDbContextAsync();
		var feedTrackers = await dbContext.FeedTrackers
			.Where(e => feedIds.Contains(e.Id))
			.ToListAsync();

		foreach(var feed in feeds)
		{
			logger.LogDebug("Checking {feed}", feed.Id);

			var newTracker = QueueFeedUpdates(feedTrackers, feed);
			if (newTracker is not null)
				dbContext.Add(newTracker);
		}

		logger.LogDebug("Feed check complete.");

		await dbContext.SaveChangesAsync();

		_ = Task.Run(SendEmails);
	}

	FeedTracker? QueueFeedUpdates(List<FeedTracker> trackers, FeedConfig feed)
	{
		var tracker = trackers.Where(e => e.Id == feed.Id)
			.FirstOrDefault();

		FeedTracker? newTracker = null;
		if (tracker is null)
		{
			newTracker = tracker = new()
			{
				Id = feed.Id,
			};
		}

		if ((DateTimeOffset.UtcNow - tracker.LastFeedCheck).TotalMilliseconds < feed.CheckMilliseconds)
		{
			logger.LogDebug("{feed} is not old enough, aborting check.", feed.Id);
			return null;
		}

		tracker.LastFeedCheck = DateTimeOffset.UtcNow;

		SyndicationFeed syndFeed;
		using (var reader = XmlReader.Create(feed.FeedUrl))
			syndFeed = SyndicationFeed.Load(reader);

		DateTimeOffset lastUpdate = tracker.LastUpdate;
		foreach (SyndicationItem item in syndFeed.Items
			.Where(e => e.PublishDate > tracker.LastUpdate))
		{
			logger.LogDebug("Queued {item} from {feed} for email.", item.Title.Text, feed.Id);

			emailQueue.Push((item, feed));
			if (lastUpdate < item.PublishDate)
				lastUpdate = item.PublishDate;
		}

		tracker.LastUpdate = lastUpdate;

		return newTracker;
	}

	async Task SendEmails()
	{
		if (emailsRunning || emailQueue.Count == 0)
			return;
		emailsRunning = true;

		try
		{
			logger.LogInformation("Starting email sender.");

			int batchSize = 0;

			while (emailQueue.TryPop(out var pair))
			{
				if (MailboxAddress.TryParse(emailConfig.Email, out var sender))
				{
					var message = new MimeMessage();
					message.From.Add(sender);

					foreach (var email in pair.cfg.EmailTo)
						if (MailboxAddress.TryParse(email, out var receiver))
							message.To.Add(receiver);

					message.Subject = "[RSS] " + pair.item.Title.Text;
					message.Body = new TextPart("html")
					{
						Text = $"<a href=\"{pair.item.Links.FirstOrDefault()?.Uri}\">Click Here</a>"
					};

					try
					{
						if (!emailClient.IsConnected)
						{
							await emailClient.ConnectAsync(emailConfig.Client, emailConfig.Port, false);
							if (emailConfig.RequireLogin)
								await emailClient.AuthenticateAsync(emailConfig.User, emailConfig.Password);
						}

						await emailClient.SendAsync(message);

						batchSize++;
					}
					catch (Exception ex)
					{
						logger.LogError(ex, $"An email failed to send.");
					}
				}
				else
				{
					throw new Exception("The configured email is invalid.");
				}

				// so we dont spam things.
				await Task.Delay(TimeSpan.FromSeconds(emailConfig.EmailDelaySeconds));
			}

			logger.LogInformation("Sent {batchSize} emails.", batchSize);
		}
		catch (Exception ex)
		{
			logger.LogError(ex, "Failed to send email batch.");
		}
		finally
		{
			emailsRunning = false;
		}
	}

	protected virtual void Dispose(bool disposing)
	{
		if (!disposedValue)
		{
			if (disposing)
			{
				rssCheckRunner.Dispose();
			}

			feeds = null!;
			disposedValue = true;
		}
	}

	public void Dispose()
	{
		// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
		Dispose(disposing: true);
		GC.SuppressFinalize(this);
	}
}
