﻿using FFEmailFeeder.Data;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFEmailFeeder.Database;

internal class DesignTimeContextFactory : IDesignTimeDbContextFactory<FeedDbContext>
{
	public FeedDbContext CreateDbContext(string[] args)
	{
		var builder = new DbContextOptionsBuilder<FeedDbContext>();
		builder.UseSqlite("Data Source=Config\\design.db");

		return new FeedDbContext(builder.Options);
	}
}

internal class FeedDbContext(DbContextOptions options) : DbContext(options)
{
	public DbSet<FeedTracker> FeedTrackers { get; set; }

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<FeedTracker>(ctx =>
		{
			ctx.HasKey(e => e.Id);
		});
	}
}
