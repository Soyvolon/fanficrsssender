﻿using FFEmailFeeder.Database;
using FFEmailFeeder.Services;

using MailKit.Net.Smtp;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Serilog;

namespace FFEmailFeeder;

internal class Program
{
	static async Task Main(string[] args)
	{
		IConfiguration config = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile(Path.Combine(Environment.CurrentDirectory, "Config", "config.json"))
			.Build();

		IServiceCollection services = new ServiceCollection();
		services.AddSingleton(config);
		services.AddLogging(builder =>
		{
			var logger = new LoggerConfiguration()
				.ReadFrom.Configuration(config)
				.CreateLogger();

			builder.AddSerilog(logger);
		});

		services.AddDbContextFactory<FeedDbContext>(options =>
			options.UseSqlite(config.GetConnectionString("Core"))
#if DEBUG
				.EnableSensitiveDataLogging()
				.EnableDetailedErrors()
#endif
				, ServiceLifetime.Singleton);

		services.AddSingleton<IFeedCheckService, FeedCheckService>();

		services.AddTransient<SmtpClient>();

		var collection = services.BuildServiceProvider();

		await RunAsync(collection);

		await Task.Delay(-1);
	}

	private static async Task RunAsync(IServiceProvider services)
	{
		using var scope = services.CreateScope();
		var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
		var dbFac = scope.ServiceProvider.GetRequiredService<IDbContextFactory<FeedDbContext>>();
		using var db = dbFac.CreateDbContext();
		await ApplyDatabaseMigrationsAsync(db);

		var feed = scope.ServiceProvider.GetRequiredService<IFeedCheckService>();
		await feed.RunAsync();
	}

	private static async Task ApplyDatabaseMigrationsAsync(DbContext database)
	{
		if (!(await database.Database.GetPendingMigrationsAsync()).Any())
		{
			return;
		}

		await database.Database.MigrateAsync();
		await database.SaveChangesAsync();
	}
}
