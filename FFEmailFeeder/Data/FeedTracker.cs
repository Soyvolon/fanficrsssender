﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFEmailFeeder.Data;
public class FeedTracker
{
	/// <summary>
	/// The ID of the feed.
	/// </summary>
	public required string Id { get; set; }

	/// <summary>
	/// The last time this feed had an update. Used to compare to feed
	/// values for sending out new emails.
	/// </summary>
	public DateTimeOffset LastUpdate { get; set; } = DateTimeOffset.MinValue;
	/// <summary>
	/// The last time this feed has been checked for updates.
	/// </summary>
	public DateTimeOffset LastFeedCheck { get; set; } = DateTimeOffset.MinValue;
}
