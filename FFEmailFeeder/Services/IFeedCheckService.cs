﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFEmailFeeder.Services;
internal interface IFeedCheckService
{
	internal Task RunAsync();
}
