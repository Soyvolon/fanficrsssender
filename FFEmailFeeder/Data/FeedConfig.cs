﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFEmailFeeder.Data;

internal class FeedConfig
{
	public required string Id { get; set; }
	public required string FeedUrl { get; set; }
	public required string FeedType { get; set; }
	public int CheckMilliseconds { get; set; } = 1800000;
	public List<string> EmailTo { get; set; } = [];
}
