﻿// <auto-generated />
using System;
using FFEmailFeeder.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace FFEmailFeeder.Migrations
{
    [DbContext(typeof(FeedDbContext))]
    partial class FeedDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "8.0.5");

            modelBuilder.Entity("FFEmailFeeder.Data.FeedTracker", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<DateTimeOffset>("LastFeedCheck")
                        .HasColumnType("TEXT");

                    b.Property<DateTimeOffset>("LastUpdate")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("FeedTrackers");
                });
#pragma warning restore 612, 618
        }
    }
}
